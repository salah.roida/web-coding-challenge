<?php
require "db.php";
require "header.php";
require "class/Vote.php";
$nearboo = false;
if(isset($_GET["page"])) {
  $page = $_GET['page'];
  if ($page == "liked"){ 
    $nearboo = false;
    $req = $pdo->prepare("SELECT * FROM votes left join shops on (votes.ref_id = shops.id) where votes.user_id = ? and votes.vote = 1");
    $req->execute([$_SESSION['user_id']]);
//$shops = $req->fetchAll();
  } 
  elseif($page == "near"){ 
    $nearboo = true;
    $user = false;
    $req = $pdo->query('SELECT * FROM shops');
    $req3 = $pdo->prepare('SELECT * FROM users WHERE id = ?');
    $req3->execute([$_SESSION['user_id']]);
    $user = $req3->fetch();
  } }
  else{
    $nearboo = false;
    $req = $pdo->query('SELECT * FROM shops');
  }
  ?>


  <!-- notification message -->
  <?php if (isset($_SESSION['success'])) : ?>
    <div class="error success" >
     <h3>
      <?php 
      echo $_SESSION['success']; 
      unset($_SESSION['success']);
      ?>
    </h3>
  </div>
<?php endif ?>

<div class="row">
  <?php foreach($req->fetchAll() as $shop): ?> <!-- get all the shops -->
  <?php
  $distance = 0;
  if($nearboo == true)
  {
    $distance = abs($user->pointA - $shop->pointB);
  } 

// get the votes for the current user for each shop
  $vote = false;
  $req2 = $pdo->prepare('SELECT * FROM votes WHERE ref = ? AND ref_id = ? AND user_id = ?');
  $req2->execute(['shops', $shop->id, $_SESSION['user_id']]);
  $vote = $req2->fetch();
  //$display = true;
  ?>
  
  <!-- Check the distance (less than 10 km) in nearby shops/ check the liked shops to not display them on home page -->
  <div class="col-sm-12 col-md-6" 

  <?php if ($nearboo == true and $distance > 10) 

  {  ?> style = 'display:none'<?php } 

  elseif (!isset($page) and isset($vote->vote))
  {
    if ($vote->vote == 1){
      ?> style = 'display:none'<?php
    }
  }
  ?>>
  <!---------------------------------------- -->
  
  <div class="shops full" >

    <img src="img/shop <?= $shop->id; ?>.png">
  </a>
</div>


<div <?php if ($nearboo == false) {  ?> style = 'display:none'<?php } ?>> Distance: <?= $distance;?> </div>
<div class="vote <?= Vote::getClass($vote); ?>" id="vote<?= $shop->id ?>" data-ref="shops" data-ref_id="<?= $shop->id ?>" data-user_id="<?= $_SESSION['user_id']?>">
  <div class="vote_btns">
    <button class="vote_btn vote_like" id="vote<?= $shop->id ?>" data-ref="shops" data-ref_id="<?= $shop->id ?>" data-user_id="<?= $_SESSION['user_id']?>""><i class="fa fa-thumbs-up"></i> <span id="like_count<?= $shop->id ?>"><?= $shop->like_count ?></span></button><!-- like counter for a shop -->
    <button class="vote_btn vote_dislike" id="vote<?= $shop->id ?>" data-ref="shops" data-ref_id="<?= $shop->id ?>" data-user_id="<?= $_SESSION['user_id']?>"><i class="fa fa-thumbs-down"></i> <span id="dislike_count<?= $shop->id ?>"><?= $shop->dislike_count ?></span></button><!-- dislike counter for a shop -->
  </div>
</div>
</div>


<?php endforeach; ?>
</div>


<?php
require "footer.php";