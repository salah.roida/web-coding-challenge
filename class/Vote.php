<?php

class Vote
{

    private $pdo;
    private $former_vote;

    public function __construct(PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    /**
     * Prepare, execute a query and fetching results
     * @param $query
     * @param $params
     * @return mixed
     */
    private function prepareAndFetch($query, $params)
    {
        $req = $this->pdo->prepare($query);
        $req->execute($params);
        return $req->fetch();
    }

    /**
     * Check if a record exists
     * @param $table
     * @param $record_id
     * @throws Exception
     */
    private function recordExists($table, $record_id)
    {
        $req = $this->pdo->prepare("SELECT * FROM $table WHERE id = ?");
        $req->execute([$record_id]);
        if ($req->rowCount() == 0) {
            throw new Exception('A Record is missing!');
        }
    }

    /**
     * Like an item
     * @param $table
     * @param $record_id
     * @param $user_id
     * @return bool
     */
    public function like($table, $record_id, $user_id)
    {
        if ($this->vote($table, $record_id, $user_id, 1)) {
            $sql_part = "";
            if ($this->former_vote) {
                $sql_part = ", dislike_count = dislike_count - 1";
            }
            $this->pdo->query("UPDATE $table SET like_count = like_count + 1 $sql_part WHERE id = $record_id");
            return true;
        } else {
            $this->pdo->query("UPDATE $table SET like_count = like_count - 1 WHERE id = $record_id");
        }
        return false;
    }

    /**
     * Dislike an item
     * @param $table
     * @param $record_id
     * @param $user_id
     * @return bool
     */
    public function dislike($table, $record_id, $user_id)
    {
        if ($this->vote($table, $record_id, $user_id, -1)) {
            $sql_part = "";
            if ($this->former_vote) {
                $sql_part = ", like_count = like_count - 1";
            }
            $this->pdo->query("UPDATE $table SET dislike_count = dislike_count + 1 $sql_part WHERE id = $record_id");
            return true;
        } else {
            $this->pdo->query("UPDATE $table SET dislike_count = dislike_count - 1 WHERE id = $record_id");
        }
        return false;
    }

    public function note($table, $record_id, $user_id, $note){
        $vote = $this->vote($table, $record_id, $user_id, $note);
        $this->updateVote($table, $record_id, true);
        return $vote;
    }

    /**
     * Fill the votes table to keep track of likes/dislikes
     * @param $table
     * @param $record_id
     * @param $user_id
     * @param $score
     * @return bool
     * @throws mixed
     */
    private function vote($table, $record_id, $user_id, $score)
    {
        $this->recordExists($table, $record_id);
        $this->former_vote = $this->prepareAndFetch("SELECT id, vote FROM votes WHERE ref=? AND ref_id=? AND user_id=?", [$table, $record_id, $user_id]);
        if ($this->former_vote) {
            // if we give the same vote we remove it (unlike, undislike)
            if ($this->former_vote->vote == $score) {
                $this->pdo->query('DELETE FROM votes WHERE id = ' . $this->former_vote->id);
                return false;
            }
            $this->pdo->prepare("UPDATE votes SET vote = ?, created_at = ? WHERE id = {$this->former_vote->id}")->execute([$score, date('Y-m-d H:i:s')]);
            return true;
        }
        return $this->pdo->prepare("INSERT INTO votes SET ref=?, ref_id=?, user_id=?, created_at=?, vote = $score")->execute([$table, $record_id, $user_id, date('Y-m-d H:i:s')]);
    }

    /**
     * update the count of like/dislike for an item
     * @param $ref string nom de la table
     * @param $ref_id integer ID de l'enregistrement
     * @return PDOStatement
     */
    public function updateLikesCount($ref, $ref_id)
    {
        $votes = $this->prepareAndFetch("SELECT COUNT(id) as count, vote FROM votes WHERE ref = ? AND ref_id = ? GROUP BY vote ", [$ref, $ref_id]);
        $counts = [
            '-1' => 0,
            '1' => 0
        ];
        foreach ($votes as $vote) {
            $counts[$vote->vote] = $vote->count;
        }
        return $this->pdo->query("UPDATE $ref SET like_count = {$counts[1]}, dislike_count = {$counts[-1]} WHERE id = $ref_id");
    }


    /**
     * Add a class is-liked or is-disliked after a record
     * @param $vote mixed false/PDORow
     * @return null|string
     */
    public static function getClass($vote)
    {
        if ($vote) {
            return $vote->vote == 1 ? 'is-liked' : 'is-disliked';
        }
        return null;
    }

}