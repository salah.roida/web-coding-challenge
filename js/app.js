$(document).ready(function(){

    var $vote;
    // if the user clicks on the like button ...
    $('.vote_like').on('click', function(){

       $vote = $(this);
       vote(1, $vote, 'like.php');
   });
    // if the user clicks on the dislike button ...
    $('.vote_dislike').on('click', function(){

       $vote = $(this);
       vote(-1, $vote, 'like.php');
   })
    function vote(value, voote, url){  //in this method we use the jqXHR object implementing the Promise interface
        $('.vote_btns', voote).hide();
        $.post(url, {
            ref: voote.data('ref'),
            ref_id: voote.data('ref_id'),
            user_id: voote.data('user_id'),
            vote: value
        }).done(function(data, textStatus, jqXHR){
            //console.log(data);
            $('#dislike_count'+voote.data('ref_id')).text(data.dislike_count);
            $('#like_count'+voote.data('ref_id')).text(data.like_count);

            if(value == 1){
                voote.addClass('is-liked');
            } else{
                voote.addClass('is-disliked');
            }



        }).fail(function(jqXHR, textStatus, errorThrown){
            alert(jqXHR.responseText);
        }).always(function(){
            $('.vote_btns', voote).fadeIn();
        });
    }
})