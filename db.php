<?php
session_start(); 
// initializing variables
$email    = "";
$user_id  = "";
$errors = array(); 

// connect to the database
$pdo = new PDO('mysql:host=localhost;dbname=challengedb', 'root', '', [
  PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
  PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
  PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ
]);

// REGISTER USER
if (isset($_POST['reg_user'])) {
  // receive all input values from the form
  $email = $_POST['email'];
  $password_1 = $_POST['password_1'];
  $password_2 = $_POST['password_2'];

  // form validation: ensure that the form is correctly filled ...
  // by adding (array_push()) corresponding error unto $errors array
  if (empty($email)) { array_push($errors, "Email is required"); }
  if (empty($password_1)) { array_push($errors, "Password is required"); }
  if ($password_1 != $password_2) {
   array_push($errors, "The two passwords do not match");
 }

  // first check the database to make sure 
  // a user does not already exist with the same email

 $req    = $pdo->query("SELECT * FROM users WHERE email = '$email'");
 $user   = $req->fetch(PDO::FETCH_ASSOC);

  if ($user) { // if user exists
    if ($user['email'] === $email) {
      array_push($errors, "email already exists");
    }
  }

  // Finally, register user if there are no errors in the form
  if (count($errors) == 0) {
  	$password = md5($password_1);//encrypt the password before saving in the database

  	$pdo->exec("INSERT INTO users (email, password) 
     VALUES('$email', '$password')");
  	
  	$_SESSION['email'] = $email;

    $req    = $pdo->query("SELECT * FROM users WHERE email = '$email'");
    $user   = $req->fetch();
    $_SESSION['user_id'] = $user->id;
    $_SESSION['success'] = "You are now logged in";
    header('location: ../index.php');
  }
}

// LOGIN USER
if (isset($_POST['login_user'])) {
  $email = $_POST['email'];
  $password = $_POST['password'];

  if (empty($email)) {
  	array_push($errors, "Email is required");
  }
  if (empty($password)) {
  	array_push($errors, "Password is required");
  }

  if (count($errors) == 0) {
  	$password = md5($password);
   $result = $pdo->prepare("SELECT SQL_CALC_FOUND_ROWS id, email FROM users WHERE email='$email' AND password='$password'"); 
   $result->execute();
   $result = $pdo->prepare("SELECT FOUND_ROWS()"); 
   $result->execute();
   $row_count =$result->fetchColumn();

   if ($row_count == 1) {
     $_SESSION['email'] = $email;
     $req    = $pdo->query("SELECT * FROM users WHERE email = '$email'");
     $user   = $req->fetch();
     $_SESSION['user_id'] = $user->id;
     $_SESSION['success'] = "You are now logged in";
     header('location: ../index.php');
   }else {
    array_push($errors, "Wrong username/password combination");
  }
}
}

