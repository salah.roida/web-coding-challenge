<?php
require 'db.php';

//Call using POST
if($_SERVER['REQUEST_METHOD'] != 'POST'){
	http_response_code(403);
	die();
}

// We launch the voting process
require 'class/Vote.php';
$vote = new Vote($pdo);
if($_POST['vote'] == 1){
	$success = $vote->like($_POST['ref'], $_POST['ref_id'], $_POST['user_id']);
}else{
	$success = $vote->dislike($_POST['ref'], $_POST['ref_id'], $_POST['user_id']);
}

$req = $pdo->prepare("SELECT like_count, dislike_count FROM {$_POST['ref']} WHERE id = ?");
$req->execute([$_POST['ref_id']]);
header('Content-type: application/json');
$record = $req->fetch(PDO::FETCH_ASSOC);
$record['success'] = $success;
die(json_encode($record));