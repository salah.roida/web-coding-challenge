**Implemented features:**

* User can sign up using my email & password
* User can sign in using my email & password
* User can display the list of shops sorted by distance, less than 10 km (it's only a simulation, dist = point A - point B)
* User can like a shop, then it will be added to my liked shops
* Liked shops aren't displayed on the main page
* User can dislike a shop
* User can display the list of liked shops
* User can remove a shop from liked shops by either disliking it or unliking it
